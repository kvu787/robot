#define PWM_1               11 // fast right
#define PWM_2               10 // slow left
#define LEFT                PWM_2
#define RIGHT               PWM_1
#define SERIAL_BAUD         9600
#define SERIAL_TIMEOUT      10
#define COMMAND_BUFFER_SIZE 64

/*
  Set the pulse for the given pin.
  Assumes that pin is either PWM_1 or PWM_2.
*/
void setCommand(int pin, int onPeriod, int offPeriod);

struct command {
  int onPeriod;
  int offPeriod;
};

struct command pwm1Command;
struct command pwm2Command;

/*
  Sends a HIGH or LOW signal to the pin depending on if the current time 
  falls within the on period or off period.

  Each period is a duration in milliseconds.
  If onPeriod == 0, always writes LOW.
  If offPeriod == 0, always writes HIGH.

  Assumes onPeriod, offPeriod >= 0.
*/
void pulseWrite(int pin, int onPeriod, int offPeriod);

/*
  Accepts a function that runs each cycle within the specified time range.
  If endTime = 0, then function will run indefinitely.
  
  Assumes:
    startTime >= 0
    startTime <= endTime
*/
void execute(int startTime, int endTime, void (*func)());

void setup() 
{
  Serial.begin(SERIAL_BAUD);
  Serial.setTimeout(SERIAL_TIMEOUT);
  
  pinMode(PWM_1, OUTPUT);
  pinMode(PWM_2, OUTPUT);
  
  setCommand(RIGHT, 0, 1);
  setCommand(LEFT, 0, 1);
}

void loop() 
{
  extern struct command pwm1Command;
  extern struct command pwm2Command;
  char side;
  int pin, onPeriod, offPeriod;
  int validCommand;
  int availableBytes;
  int i;

  // handle input

  availableBytes = Serial.available();
  if (availableBytes)
  {

    // parse commands
    side = Serial.read();
    onPeriod = Serial.parseInt();
    offPeriod = Serial.parseInt();
    validCommand = (side == 'R' || side == 'L') && onPeriod && offPeriod;

    // run command
    if (validCommand)
    {
      if (side == 'R') { pin = RIGHT; }
      else             { pin = LEFT; }

      if      (onPeriod < 0)  { setCommand(pin, 1, 0); }
      else if (offPeriod < 0) { setCommand(pin, 0, 1); }
      else                    { setCommand(pin, onPeriod, offPeriod); }
    }

    // flush incoming buffer
    for (i = 0; i < 64; ++i) { Serial.read(); }

    // report command sent
    Serial.print("valid? ");
    if (validCommand) { Serial.println("true"); }
    else              { Serial.println("false"); }
    
    Serial.print("pin: ");
    Serial.println(side);
    
    Serial.print("on period: ");
    Serial.println(onPeriod);

    Serial.print("off period: ");
    Serial.println(offPeriod);

    Serial.println("");
    Serial.flush();
  }

  // execute commands
  pulseWrite(PWM_1, pwm1Command.onPeriod, pwm1Command.offPeriod);
  pulseWrite(PWM_2, pwm2Command.onPeriod, pwm2Command.offPeriod);
}

void setCommand(int pin, int onPeriod, int offPeriod)
{
  extern struct command pwm1Command;
  extern struct command pwm2Command;
  struct command newCommand;
  
  newCommand.onPeriod = onPeriod;
  newCommand.offPeriod = offPeriod;

  if (pin == PWM_1)
  {
    pwm1Command = newCommand;
  }
  else if (pin == PWM_2)
  {
    pwm2Command = newCommand;
  }
}

void pulseWrite(int pin, int onPeriod, int offPeriod)
{
  int totalPeriod;
  int currentPosition;

  if (onPeriod == 0) {
    digitalWrite(pin, LOW);
    return;
  }

  if (offPeriod == 0)
  {
    digitalWrite(pin, HIGH);
    return;
  }
 
  totalPeriod = onPeriod + offPeriod;
  currentPosition = millis() % totalPeriod;

  if (currentPosition < offPeriod)
  {
    digitalWrite(pin, LOW);
  }
  else
  {
    digitalWrite(pin, HIGH);
  }
}

void execute(int startTime, int endTime, void (*func)())
{
  int timeMs;

  timeMs = millis();
  
  if (endTime == 0)
  {
    if (timeMs >= startTime)
    {
      (*func)();
    }
  }
  else 
  {
    if (timeMs >= startTime && timeMs <= endTime)
    {
      (*func)();
    }
  }
}